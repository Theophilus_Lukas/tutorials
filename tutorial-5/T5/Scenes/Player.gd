extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var jumpcount = 0
export (int) var maxjumpcount = 1

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	var animation = "diam_kanan"
	#if is_on_floor() and Input.is_action_just_pressed('up'):
	#	velocity.y = jump_speed
	if Input.is_action_pressed('emote') and jumpcount < maxjumpcount:
		animation = "teabag"
		
	if Input.is_action_just_pressed('up') and jumpcount < maxjumpcount:
		jumpcount+=1
		velocity.y = jump_speed
		animation = "loncat_kanan"
		
	if is_on_floor():
		jumpcount = 0
		
	if !is_on_floor():
		if Input.is_action_pressed('right'):
			velocity.x += speed
			animation = "loncat_kanan"
		elif Input.is_action_pressed('left'):
			velocity.x -= speed
			animation = "loncat_kiri"
		else:
			animation = "loncat_neut"
			
	if Input.is_action_pressed('right') and is_on_floor():
		velocity.x += speed
		animation = "jalan_kanan"
		
	if Input.is_action_pressed('left') and is_on_floor():
		velocity.x -= speed
		animation = "jalan_kiri"
		
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)



func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

